// Creating a Map must be done like this:
const namesMap = new Map();

// We HAVE TO call set/get to add/retrieve entries to/from our Map:
namesMap.set('Ann', 'Gielis');
namesMap.set('Tom', 'De Reys');
namesMap.set('Piet', 'Boedt');
console.log('Tom who? ' + namesMap.get('Tom'));

// !!! WARNING !!!
// Square brackets makes code very readable, but this does NOT
// access the Map interface in any way!
namesMap['Lars'] = 'Willemsens';
namesMap['Jan'] = 'de Rijke';
namesMap['Wim'] = 'De Keyser';
namesMap['Toni'] = 'Mini';

let firstName = 'Wim';
console.log(`${firstName}'s last name is ${namesMap[firstName]}.`);

// ... instead, properties have been added to the 'namesMap' object:
console.log(`Lars's last name is ${namesMap.Lars}.`);

// !!! WARNING !!!
// As stated in 'Eloquent JavaScript', this is an Object, and NOT a Map:
const theCompany = {
    'Antoon': 'Stessels',
    'Dieter': 'De leeuw',
    'Charmaine': 'Pavloskova'
};
// Even with objects, we can use square brackets:
console.log('Mrs. ' + theCompany['Charmaine'] + '\n');

// We'll use an iterator independant of 'Symbol'.
// Iterators can be used to iterate over iterables:
const firstNamesIterator = namesMap.keys();
console.log('Lars, Jan, Wim, and Toni are NOT keys of the Map. Why not?')
for (let firstName of firstNamesIterator) {
    console.log(firstName);
}

// !!! WARNING !!!
// "for ... of", which works with iterables is NOT to be confused with
// "for ... in", which goes over all (non-Symbol) properties.
